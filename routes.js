
const requestHandler = ((req, res) => {
    console.log(req.url, req.method, req.headers);
    const url = req.url;
    const method = req.method;

    if (url === '/') {
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({ message: "Hello from node Server js" }))
    }
    if (url === '/message' && method === 'POST') {
        const body = [];
        req.on('data', (chunk) => {
            console.log("chunk", chunk);
            body.push(chunk);
        });
        req.on('end', () => {
            const parseBody = Buffer.concat(body).toString();
            console.log("parseBody", parseBody);
            return res.end(parseBody);//to end the request
        });
    }
})

module.exports = requestHandler; 

// module.exports = {
//     handler : requestHandler,
//     someText:"SOME TEXT"
// }

// module.exports.handler = requestHandler;
// exports.handler = requestHandler;
